# [1.3.0](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/compare/v1.2.0...v1.3.0) (2021-08-24)


### Features

* select programming language count by developer ([3457d79](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/3457d79a7e40ff3afd62f76c1825521496a76208))

# [1.2.0](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/compare/v1.1.0...v1.2.0) (2021-08-24)


### Features

* add select to show all non key columns ([a14c6aa](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/a14c6aa09f4da137c94486a07357026e866a69ba))

# [1.1.0](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/compare/v1.0.0...v1.1.0) (2021-08-23)


### Features

* add records to developer table ([0d79c19](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/0d79c19a2f3a8b089830e26682936c67bdc0f6ec))
* add records to developer-language associative table ([68279f9](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/68279f9f851be92a8be50b78fc8b624f659db7e1))
* add records to programming language table ([2db0063](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/2db006309991f3caca8cf87d7dc34ccf9a96b734))

# 1.0.0 (2021-08-23)


### Features

* add developer-language associative entity ([bdd7887](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/bdd78874bcad8e3ab6be4c5523baf940decc3982)), closes [#1](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/issues/1)
* add programming language table ([61cddfe](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/61cddfe9e44154b23fad37dd36f09aee26a0e993))
* create database ([c07bca8](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/c07bca81d9149ac4ba705135781854a666ea0917))
* create developer table ([86ad904](https://gitlab.com/intera-o-humano-computador-2021-2/5.-revisao-relacao-n-n/commit/86ad90497e7265664cba5baefe9a359db309694f))
