SELECT desenvolvedor.nome,
    desenvolvedor.data_nascimento,
    linguagem_programacao.nome,
    linguagem_programacao.versao
FROM desenvolvedor
    JOIN desenvolvedor_linguagem_programacao ON desenvolvedor.id = desenvolvedor_linguagem_programacao.id_desenvolvedor
    JOIN linguagem_programacao ON linguagem_programacao.id = desenvolvedor_linguagem_programacao.id_linguagem_programacao;