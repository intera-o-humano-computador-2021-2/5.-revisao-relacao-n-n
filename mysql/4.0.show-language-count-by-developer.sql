SELECT desenvolvedor.nome AS desenvolvedor,
    count(linguagem_programacao.id) AS qtd_linguagem_programacao
FROM desenvolvedor
    JOIN desenvolvedor_linguagem_programacao ON desenvolvedor.id = desenvolvedor_linguagem_programacao.id_desenvolvedor
    JOIN linguagem_programacao ON linguagem_programacao.id = desenvolvedor_linguagem_programacao.id_linguagem_programacao
GROUP BY desenvolvedor.nome;