DROP TABLE IF EXISTS desenvolvedor;
CREATE TABLE desenvolvedor(
    id INT AUTO_INCREMENT NOT NULL,
    nome VARCHAR(255) NOT NULL,
    data_nascimento DATE NOT NULL,
    PRIMARY KEY(id)
);