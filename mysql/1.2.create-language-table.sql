DROP TABLE IF EXISTS linguagem_programacao;
CREATE TABLE linguagem_programacao(
    id INT AUTO_INCREMENT NOT NULL,
    nome VARCHAR(255) NOT NULL,
    versao VARCHAR(10) NOT NULL,
    PRIMARY KEY(id)
);