DROP TABLE IF EXISTS desenvolvedor_linguagem_programacao;
CREATE TABLE desenvolvedor_linguagem_programacao(
    id INT AUTO_INCREMENT NOT NULL,
    id_desenvolvedor INT NOT NULL,
    id_linguagem_programacao INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (id_desenvolvedor) REFERENCES desenvolvedor(id),
    FOREIGN KEY (id_linguagem_programacao) REFERENCES linguagem_programacao(id)
);