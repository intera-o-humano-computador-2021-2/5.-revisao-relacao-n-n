# 5. Revisão: Relação N-N

Resolução da [atividade 5](https://classroom.google.com/c/MzcwNzExMDU1ODQ1/a/Mzc1NzcwODAyODYw/details)

## Guia de uso

Siga os passos a seguir para replicar a resolução da atividade

### Instalação

Adicione as extensões recomendadas para a workspace

Crie o arquivo `.env` seguindo o esquema definido em `.env.example`

### Uso

Para criar uma instancia do MySQL, execute o comando:

```bash
 $ docker-compose up -d --build
```

Para replicar as ações realizadas no banco, execute os comandos presentes na pasta `mysql` em ordem numérica.

## Autores

- [@fellipe.vidal](https://www.gitlab.com/fellipe.vidal)
